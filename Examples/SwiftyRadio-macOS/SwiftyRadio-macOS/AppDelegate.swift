//
//  AppDelegate.swift
//  SwiftyRadio-macOS
//
//  Created by Eric Conner on 6/13/17.
//  Copyright © 2017 Eric Conner Apps. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
}
